# -*- coding: utf-8 -*-
import setuptools

# from pkg_resources import Requirement, resource_filename
# filename = resource_filename(Requirement.parse("rhonda_test"),"config/logging_config.conf")


with open("README.md", "r") as fh:
    long_description = fh.read()
setuptools.setup(
    name='rhonda_test',
    version='0.1',
    scripts=['install'],
    author="Svyatoslav Postevoy",
    author_email="postevoy.svyatoslav@gmail.com",
    description="Test task for Rhonda Software",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/mrakolice/rhonda_test",
    packages=setuptools.find_packages(),
    include_package_data = True,
    install_requires=[
        'graphics.py'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
