# rhonda_test


Работа пакета проверялась на Ubuntu 18.04.
Для корректной работы необходимо установить пакет python3-tk

    apt-get install python3-tk
    
Приложение собирается в pip пакет, который можно установить как зависимость.

    python setup.py bdist_wheel
    pip install dist/rhonda_test-{version}-py3-none-any.whl
     
Текущая версия пакета - 0.1

##Существующие проблемы приложения:
1. Возможные SQL Injection при записи данных\получении данных (потому что используется str.format для команды, отправляемой в SqLite)
2. Лог-файл создается в директории, где был вызван интерпретатор питона
3. После каждой итерации память не очищается, поэтому возможно значительное увеличение памяти при работе приложения
4. Криво написанные условия (не Pythonic way) с переменными **enable_step_by_step** и **enable_database_logging**
5. Не реализованы функции получения ИДов пробегов и итераций из БД
6. Не реализована проверка входящих данных в функциях **DatabaseService.get_triangles_for_run** и **DatabaseService.get_triangles_for_iteration**
7. Не реализована проверка количества элементов в массиве точек при инициализации объекта **Triangle**
7. Не реализовано логгирование в БД при вызове функции **TriangleService.calculate_one_iteration**
8. Невозможно изменить местонахождение базы данных, не изменяя исходный код **DatabaseService.__get_connection**
9. Нежелательно использование количества итераций больше 10-12 из-за большого времени выполнения расчетов

## Примеры использования:

### Базовое использование
    from rhonda_test import TriangleService
    TriangleService().setup_screen(width, height).calculate(iteration_count, curve_coefficient)
    
**width** и **height** должны быть целыми числами больше нуля

**iteration_count** - целое число больше 0

**curve_coefficient** - коэффициент кривизны, дробное число в диапазоне от 0 до 1 (границы не включены)

### Установка флагов enable_step_by_step и enable_database_logging
    from rhonda_test import TriangleService
    enable_step_by_step = True
    enable_database_logging = True
    TriangleService(enable_step_by_step, enable_database_logging).setup_screen(width, height).calculate(iteration_count, curve_coefficient)
    
Установка флага **enable_step_by_step** позволяет просмотреть каждый промежуточный результат. После каждого отображения текущего состояния необходимо щелкнуть мышью по рабочей области.

По умолчанию флаг стоит в **False**

Установка флага **enable_database_logging** включает или выключает логгирование сущностей в SqLite базу данных, находящуюся внутри папки пакета под путем 

    {packages-path}/rhonda_test/db/triangles.db

### Получение информации из БД

Реализованы функции получения информации из базы данных по пробегу (запуск**TriangleService.calculate**) и по номеру итерации для известного пробега.

    from rhonda_test import TriangleService, DatabaseService
    run_id = 1
    iteration_id = 1
    with DatabaseService() as database:
        all_triangles_in_run = database.get_triangles_for_run(run_id)
        all_triangles_iteration = database.get_triangles_for_iteration(iteration_id)

### Функция вычисления одного шага итерации

    from rhonda_test import TriangleService, Triangle
    triangles = [
        Triangle([
            [x1,y1],
            [x2,y2],
            [x3,y3],
        ]),
        ...
    ]
    TriangleService().setup_screen(width, height).calculate_one_iteration(triangles, curve_coefficient)
    