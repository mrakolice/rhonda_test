# -*- coding: utf-8 -*-

class BaseException(Exception):
    def __str__(self):
        return self.__repr__()


class ArgumentMustBeInt(BaseException):
    def __init__(self, argument_number):
        self.argument_number = argument_number

    def __repr__(self):
        return 'Argument [{}] must be Int'.format(self.argument_number)


class ArgumentMustBeAboveZero(BaseException):
    def __init__(self, argument_number):
        self.argument_number = argument_number

    def __repr__(self):
        return 'Argument [{}] must be above zero'.format(self.argument_number)


class ArgumentMustBeBelowNumber(BaseException):
    def __init__(self, number, argument_number):
        self.number = number
        self.argument_number = argument_number

    def __repr__(self):
        return 'Argument [{}] must be below {}'.format(self.argument_number, self.number)


class FieldNotExist(BaseException):
    def __init__(self, field_name):
        self.field_name = field_name

    def __repr__(self):
        return 'Field "{}" not initialized correctly'.format(self.field_name)


class NotPoint(BaseException):
    def __init__(self, wrong_data):
        self.wrong_data = wrong_data

    def __repr__(self):
        return 'Data "{}" is not a point'.format(self.wrong_data)


class NotList(BaseException):
    def __init__(self, wrong_data):
        self.wrong_data = wrong_data

    def __repr__(self):
        return 'Data "{}" is not a list'.format(self.wrong_data)