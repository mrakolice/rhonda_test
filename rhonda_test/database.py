# -*- coding: utf-8 -*-

import sqlite3
from datetime import datetime

from .decorators import should_be_defined_class_fields

from rhonda_test.entities import Triangle, Point

'''
    Класс, в котором инкапсулируется работа с базой данных
'''
class DatabaseService(object):
    __connection = None
    __cursor = None

    def __init__(self):
        self.__create_tables()
        self.run_id = None
        self.iteration_id = None
        self.triangle_id = None
        self.point_id = None

    def __enter__(self):
        self.__get_cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.__cursor:
            self.commit()
            self.__close_connection()

    def __get_connection(self):
        if self.__connection is None:
            from os import path
            database_file_path = path.join(path.dirname(path.abspath(__file__)), 'db/triangles.db')
            self.__connection = sqlite3.connect(database_file_path)
        return self.__connection

    def __get_cursor(self):
        if self.__cursor is None:
            self.__cursor = self.__get_connection().cursor()
        return self.__cursor

    def __close_connection(self):
        self.__get_connection().close()


    '''
        Исполнение запроса в БД (не select)
    '''
    def execute(self, execute_string):
        self.__get_cursor().execute(execute_string)

    '''
        Исполнение запроса в БД, связанного с выборкой данных
    '''
    def select(self, select_string):
        self.execute(select_string)
        all_values = self.__get_cursor().fetchall()
        names = [description[0] for description in self.__get_cursor().description]
        return [dict(zip(names, values)) for values in all_values]

    '''
        Сохранение данных в БД (завершение транзакции)
    '''
    def commit(self):
        self.__get_connection().commit()

    '''
        Создание требуемых таблиц в БД
    '''
    def __create_tables(self):
        # Run table
        self.execute('''
CREATE TABLE IF NOT EXISTS runs (
 id INTEGER PRIMARY KEY,
 date TEXT
);
        ''')

        # Iterations Table
        self.execute('''
CREATE TABLE IF NOT EXISTS iterations (
 id INTEGER PRIMARY KEY,
 iteration_number INTEGER,
 run_id INTEGER,
  FOREIGN KEY(run_id) REFERENCES runs(id) ON DELETE CASCADE ON UPDATE CASCADE
);
        ''')

        # Triangle table
        self.execute('''
CREATE TABLE IF NOT EXISTS triangles (
 id INTEGER PRIMARY KEY,
 iteration_id INTEGER,
  FOREIGN KEY(iteration_id) REFERENCES iterations(id) ON DELETE CASCADE ON UPDATE CASCADE
);
        ''')

        # Points table
        self.execute('''
CREATE TABLE IF NOT EXISTS points (
 id INTEGER PRIMARY KEY,
 triangle_id INTEGER,
 x REAL,
 y REAL,

  FOREIGN KEY(triangle_id) REFERENCES triangles(id) ON DELETE CASCADE ON UPDATE CASCADE
);
        ''')

        # Edges table
        self.execute('''
        CREATE TABLE IF NOT EXISTS edges (
         id INTEGER PRIMARY KEY,
         triangle_id INTEGER,
         start_point_id INTEGER,
         end_point_id INTEGER,
          FOREIGN KEY(triangle_id) REFERENCES triangles(id) ON DELETE CASCADE ON UPDATE CASCADE ,
          FOREIGN KEY(start_point_id) REFERENCES points(id) ON DELETE CASCADE ON UPDATE CASCADE ,
          FOREIGN KEY(end_point_id) REFERENCES points(id) ON DELETE CASCADE ON UPDATE CASCADE
        );
                ''')

        self.commit()

    '''
        Сохранение ИД запуска во внутреннее поле для решения проблемы с Insert ... Returning Id
    '''
    def __save_run_id(self):
        self.run_id = self.__get_cursor().lastrowid

    '''
        Сохранение ИД итерации во внутреннее поле для решения проблемы с Insert ... Returning Id
    '''
    def __save_iteration_id(self):
        self.iteration_id = self.__get_cursor().lastrowid

    '''
        Сохранение ИД треугольника во внутреннее поле для решения проблемы с Insert ... Returning Id
    '''
    def __save_triangle_id(self):
        self.triangle_id = self.__get_cursor().lastrowid

    '''
        Сохранение ИД точки во внутреннее поле для решения проблемы с Insert ... Returning Id
    '''
    def __save_point_id(self):
        self.point_id = self.__get_cursor().lastrowid

    '''
        Создать сущность "Запуск"
    '''
    def add_run(self, date=None):

        if date is None:
            date = datetime.now()

        self.execute('''
        INSERT INTO runs(date) VALUES ('{}');
        '''.format(str(date)))

        self.commit()
        self.__save_run_id()

    '''
        Создать сущность "Итерация"
    '''
    @should_be_defined_class_fields('run_id')
    def add_iteration(self, iteration_number):
        self.execute('''
        INSERT INTO iterations(iteration_number, run_id) VALUES ({}, {});
        '''.format(iteration_number, self.run_id))
        self.commit()
        self.__save_iteration_id()

    '''
        Создать сущность "треугольник"
    '''
    @should_be_defined_class_fields('iteration_id')
    def add_triangle(self, triangle):
        self.execute('''
        INSERT INTO triangles(iteration_id) VALUES ({});
        '''.format(self.iteration_id))
        self.commit()
        self.__save_triangle_id()

        point_ids = []
        for point in triangle.points:
            self.add_point(point)
            point_ids.append(self.point_id)

        self.add_edge(point_ids[0], point_ids[1])
        self.add_edge(point_ids[1], point_ids[2])
        self.add_edge(point_ids[0], point_ids[2])

    '''
        Создать сущность "Точка"
    '''
    @should_be_defined_class_fields('triangle_id')
    def add_point(self, point):
        self.execute('''
        INSERT INTO points(triangle_id, x, y) VALUES ({}, {}, {});
        '''.format(self.triangle_id, point.x, point.y))
        self.commit()
        self.__save_point_id()

    '''
        Создать сущность "Ребро"
    '''
    @should_be_defined_class_fields('triangle_id')
    def add_edge(self, start_point_id, end_point_id):
        self.execute('''
        INSERT INTO edges(triangle_id, start_point_id, end_point_id) VALUES ({}, {}, {});
        '''.format(self.triangle_id, start_point_id, end_point_id))
        self.commit()

    '''
        Вспомогательная функция для преобразования приходящих точек в сущность "Треугольник"
    '''
    def __parse_points_to_triangles(self, select_string):
        dict_points = self.select(select_string)

        triangle_ids = set(map(lambda x: x['triangle_id'], dict_points))
        points_by_triangles = [[y for y in dict_points if y['triangle_id'] == x] for x in triangle_ids]

        triangles = [Triangle([Point.from_dict(dict_point) for dict_point in grouped_points]) for grouped_points in
                     points_by_triangles]

        return triangles

    '''
        Возвращает все треугольники, созданные при запуске, без деления на итерации
    '''
    def get_triangles_for_run(self, run_id):
        return self.__parse_points_to_triangles(
            '''select points.x, points.y, points.triangle_id from points
              INNER JOIN triangles on points.triangle_id = triangles.id
              INNER JOIN iterations on triangles.iteration_id = iterations.id
            where iterations.run_id = {};
                    '''.format(run_id)
        )

    '''
        Возвращает все треугольники, созданные при одной итерации
    '''
    def get_triangles_for_iteration(self, iteration_id):
        return self.__parse_points_to_triangles(
            '''select points.x, points.y, points.triangle_id from points
              INNER JOIN triangles on points.triangle_id = triangles.id
              INNER JOIN iterations on triangles.iteration_id = iterations.id
            where triangles.iteration_id = {};
                    '''.format(iteration_id)
        )
