# -*- coding: utf-8 -*-

import logging
import logging.config

from rhonda_test import exceptions

'''
    Декоратор, задача которого - проверка входящих аргументов на то, что они больше 0
'''


class arguments_must_be_above_zero(object):
    def __init__(self, is_class_method=False):
        self.is_class_method = is_class_method

    def __call__(self, func):
        def wrapped_func(*args, **kwargs):
            check_args = args
            # if decorator calls in class method, skip self argument
            if self.is_class_method:
                check_args = args[1:]

            for index, arg in enumerate(check_args):
                if arg < 0:
                    raise exceptions.ArgumentMustBeAboveZero(index)
            for key, value in kwargs:
                if value < 0:
                    raise exceptions.ArgumentMustBeAboveZero(key)

            original_result = func(*args, **kwargs)

            return original_result

        return wrapped_func


'''
    Декоратор, задача которого - проверка входящего аргумента на то, что он меньше определенного числа
    number - число (верхняя граница)
    argument_number - индекс аргумента (self учитывается)
'''


class argument_must_be_below(object):
    def __init__(self, number, argument_number):
        self.number = number
        self.argument_number = argument_number
        self.is_arg = isinstance(argument_number, int)
        self.is_kwarg = isinstance(argument_number, str)

    def __call__(self, func):
        def wrapped_func(*args, **kwargs):
            # if decorator calls in class method, skip self argument
            if self.is_arg and args[self.argument_number] > self.number:
                raise exceptions.ArgumentMustBeBelowNumber(self.number, self.argument_number)

            if self.is_kwarg and kwargs.get(self.argument_number) > self.number:
                raise exceptions.ArgumentMustBeBelowNumber(self.number, self.argument_number)

            original_result = func(*args, **kwargs)

            return original_result

        return wrapped_func


'''
    Декоратор, задача которого - проверка на то, определены ли те или иные поля класса
'''


class should_be_defined_class_fields(object):
    def __init__(self, *args):
        self.fields = args

    def __call__(self, func):
        def wrapped_func(*args, **kwargs):
            # Check in self attribute
            for field in self.fields:
                if getattr(args[0], field, None) is None:
                    raise exceptions.FieldNotExist(field)

            original_result = func(*args, **kwargs)

            return original_result

        return wrapped_func


'''
    Декоратор, задача которого - логгировать в файл как входные, так и выходные данные
'''


class log_to_file(object):
    def __init__(self):
        from os import path
        log_file_path = path.join(path.dirname(path.abspath(__file__)), 'config/logging_config.conf')
        logging.config.fileConfig(log_file_path)
        self.logger = logging.getLogger("triangles")

    def __call__(self, func):
        def wrapped_func(*args, **kwargs):
            # Check in self attribute
            self.logger.info('Args: {}, Kwargs: {}'.format(args, kwargs))

            try:
                original_result = func(*args, **kwargs)
                self.logger.info('Result: {}'.format(original_result))
                return original_result
            except Exception as e:
                self.logger.error('{}'.format(e))
                raise e

        return wrapped_func
