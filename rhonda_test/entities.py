# -*- coding: utf-8 -*-
from random import triangular

from graphics import Point as GraphicPoint, Polygon

from rhonda_test.decorators import log_to_file
from rhonda_test.exceptions import NotPoint, NotList

'''
    Точка на плоскости
'''
class Point:
    __slots__ = ('x', 'y')

    def __init__(self, x: float = 0, y: float = 0):
        self.x = x
        self.y = y

    @log_to_file()
    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    @log_to_file()
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    @log_to_file()
    def __truediv__(self, other):
        return Point(self.x / other, self.y / other)

    def __repr__(self):
        return '({};{})'.format(self.x, self.y)

    '''
        Конвертирует этот класс в класс, пригодный для использования в graphics.py
    '''
    def to_graphic_point(self):
        return GraphicPoint(self.x, self.y)

    '''
        Проверяет, является ли переданный аргумент точкой и возвращает объект Point
    '''
    @staticmethod
    @log_to_file()
    def check_point(point):
        if isinstance(point, Point):
            return point
        if isinstance(point, list):
            if len(point) == 2:
                return Point(point[0], point[1])
            raise NotPoint(point)
        raise NotPoint(point)

    '''
        Проверяет, являются ли все элементы переданного списка объектами Point
    '''
    @staticmethod
    @log_to_file()
    def check_points(points):
        # TODO exceptions
        if not isinstance(points, list):
            raise NotList(points)

        return [Point.check_point(p) for p in points]

    '''
        Создает точку из переданного словаря
    '''
    @staticmethod
    def from_dict(dict_point):
        return Point(dict_point['x'], dict_point['y'])

'''
    Ребро треугольника (или отрезок)
'''
class Edge:
    __slots__ = ('start_point', 'end_point')

    def __init__(self, start_point: Point = None, end_point: Point = None):
        self.start_point = start_point
        self.end_point = end_point


    '''
        Возвращает случайную точку, находящуюся около середины ребра
    '''
    @log_to_file()
    def get_random_point(self, curve_coefficient):
        randomX = triangular(-curve_coefficient, curve_coefficient)
        randomY = triangular(-curve_coefficient, curve_coefficient)
        proectionX = abs(self.end_point.x - self.start_point.x)
        proectionY = abs(self.end_point.y - self.start_point.y)

        middle_pointX = self.start_point.x + (self.end_point.x - self.start_point.x) / 2
        middle_pointY = self.start_point.y + (self.end_point.y - self.start_point.y) / 2

        return Point(middle_pointX + randomX * proectionX, middle_pointY + randomY * proectionY)

    def __repr__(self):
        return '{} - {}'.format(self.start_point, self.end_point)

'''
    Треугольник
'''
class Triangle:
    __slots__ = ('points', 'edges')

    def __init__(self, points, disable_check=False):
        if disable_check:
            self.points = points
        else:
            self.points = Point.check_points(points)

        self.edges = (
            Edge(self.points[0], self.points[1]),
            Edge(self.points[1], self.points[2]),
            Edge(self.points[0], self.points[2]),
        )

    '''
        Функция отрисовки треугольника на переданном объекте типа GraphWin
    '''
    def draw(self, screen):
        p = Polygon(list(map(lambda point: point.to_graphic_point(), self.points)))
        p.draw(screen)

    def __repr__(self):
        points = 'Points: {}'.format(", ".join(str(point) for point in self.points))
        edges = 'Edges: {}'.format("; ".join(str(edge) for edge in self.edges))
        return '''{}
        {}
        '''.format(points, edges)


    '''
        Возвращает случайные точки для каждого ребра треугольника
    '''
    @log_to_file()
    def get_new_points(self, curve_coefficient):
        return [edge.get_random_point(curve_coefficient) for edge in self.edges]

    '''
        Создает новые треугольники на основе текущего
    '''
    @log_to_file()
    def create_new_triangles(self, curve_coefficient):
        # для каждого треугольника создаем новые вершины
        new_points = self.get_new_points(curve_coefficient)
        # создаем новые треугольники, где каждый новый треугольник берется из начальной вершины
        # и двух вершин, находящихся на смежных ребрах

        return (
            # 1 вершина, 1 ребро, 2 ребро
            Triangle([self.points[0], new_points[0], new_points[2], ], True),
            # 2 ребро, 2 вершина, 3 ребро
            Triangle([new_points[0], self.points[1], new_points[1], ], True),
            # 1 ребро, 3 вершина, 3 ребро
            Triangle([new_points[1], self.points[2], new_points[2], ], True)
        )