# -*- coding: utf-8 -*-

from typing import List

from .decorators import arguments_must_be_above_zero, should_be_defined_class_fields, argument_must_be_below, log_to_file
from .entities import Triangle
from graphics import GraphWin

from rhonda_test.database import DatabaseService


'''
    Основная точка входа приложения
'''
class TriangleService:
    __slots__ = ('width', 'height', 'iteration_count', 'triangles', 'enable_step_by_step', 'enable_database_logging')

    def __init__(self, enable_step_by_step:bool=False, enable_database_logging:bool=True):
        self.enable_step_by_step = enable_step_by_step
        self.enable_database_logging = enable_database_logging
        self.width = None
        self.height = None
        self.iteration_count = None
        self.triangles = None


    '''
        Задаем размеры экрана
    '''
    @log_to_file()
    @arguments_must_be_above_zero(True)
    def setup_screen(self, width: int, height: int):
        self.width = width
        self.height = height
        return self

    '''
        Вычисляем одну итерацию по созданию новых треугольников на основе старых
    '''
    @log_to_file()
    def calculate_one_iteration(self, triangles: List[Triangle], curve_coefficient: float):
        return [item for x in triangles for item in x.create_new_triangles(curve_coefficient)]


    '''
        Вычисляем все итерации, отрисовываем данные
    '''
    @log_to_file()
    @argument_must_be_below(1.0, 2)
    @arguments_must_be_above_zero(True)
    @should_be_defined_class_fields('width', 'height')
    def calculate(self, iteration_count: int, curve_coefficient: float):
        # создаем начальный треугольник
        self.iteration_count = iteration_count
        self.triangles = [
            Triangle([
                [0, self.height],
                [self.width / 2, 0],
                [self.width, self.height]
            ])
        ]

        window = GraphWin("Rhonda Test", self.width, self.height, False)

        if self.enable_step_by_step:
            for triangle in self.triangles:
                triangle.draw(window)
            window.update()
            window.getMouse()

        with DatabaseService() as database:
            if self.enable_database_logging:
                database.add_run()
        # пока не закончатся итерации, создаем новые треугольники (фактически, новые вершины)
            for i in range(0, iteration_count):

                if self.enable_database_logging:
                    database.add_iteration(i)

                # Бежим по треугольникам
                # temp_triangles = []
                self.triangles = self.calculate_one_iteration(self.triangles, curve_coefficient)

                if self.enable_step_by_step:
                    for item in window.items[:]:
                        item.undraw()
                    window.update()
                    for triangle in self.triangles:
                        triangle.draw(window)
                    window.update()
                    window.getMouse()

                if self.enable_database_logging:
                    for triangle in self.triangles:
                        database.add_triangle(triangle)

        if not self.enable_step_by_step:
            for triangle in self.triangles:
                    triangle.draw(window)

            window.getMouse()  # Pause to view result

        window.close()  # Close window when done

        return self.triangles

